#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        vector<int> alpha;
    
    public:
        Engine()
        {
            for(int i = 0 ; i < 26 ; i++)
            {
                alpha.push_back(-1);
            }
        }
    
        char findFirstNonRepeatingCharacterInString(string str)
        {
            int  len = (int)str.length();
            char output;
            for(int i = 0 ; i < len ; i++)
            {
                alpha[str[i] - 'A']++;
            }
            
            for(int i = 0 ; i < len ; i++)
            {
                if(!alpha[str[i] - 'A'])
                {
                    output = str[i];
                    break;
                }
            }
            
            return output;
        }
};

int main(int argc, const char * argv[])
{
    string str = "ADBCGHIEFKJLADTVDERFSWVGHQWCNOPENSMSJWIERTFB";
//    string str = "ABCDKBADC";
    Engine e = Engine();
    cout<<e.findFirstNonRepeatingCharacterInString(str)<<endl;
    return 0;
}
